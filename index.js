const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const connection = require('./database/database');
const Pergunta = require('./database/Pergunta');
const Resposta = require('./database/Resposta');
//Database

connection.authenticate()
    .then(() => {
        console.log("Connection was established");
    }).catch((err) => {
        console.log("Error: " + err);
    });

//Informando ao express que vou utilizar o ejs como view engine
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: false}));

app.get('/', (req, res) => {
    Pergunta.findAll({ raw: true, order: [['id','DESC']] }).then((response) => {
        res.render("index", {
            perguntas: response
        });
    }).catch((err) => {
        console.log(err);
    });
});

app.get('/criar-pergunta', (req, res) => {
    res.render("criar-perguntas");
});

app.post('/salvar-pergunta', (req, res) => {
    let titulo = req.body.titulo;
    let descricao = req.body.descricao; // -> USAR BODY
    Pergunta.create({
        titulo: titulo,
        descricao: descricao
    }).then(() => {
        res.redirect('/');
    });
});

app.get('/pergunta/:id', (req, res) => {
    var id = req.params.id;
    Pergunta.findOne({
        where: {id: id}
    }).then((pergunta) => {
         if (pergunta) {
            Resposta.findAll({
                where: {perguntaId: pergunta.id},
                order: [['id', 'DESC']],
            }).then((respostas) => {
                res.render('pergunta', {
                    pergunta: pergunta,
                    respostas: respostas
                });
            });
         } else {
            res.redirect('/');
         }
    }).catch((err) => {
        console.log(err);
    });
});

app.post('/responder', (req, res) => {
    var corpo = req.body.corpo;
    var perguntaId = req.body.pergunta;
    Resposta.create({
        corpo: corpo,
        perguntaId: perguntaId,
    }).then(() => {
        res.redirect(`/pergunta/${perguntaId}`)
    }).catch((err) => {
        console.log(err);
    });
});

app.listen(3000, () => {
    console.log('Listening on port 3000');
});